Welcome to Content Tools TodoList!
===================


***Never miss a Task***

----------


Requirements
-------------

> - Browser with html5 support.


Instructions
-------------

> - [Click here to download a zip file](https://gitlab.com/joelgarciajr84/todolist-content-tools/repository/master/archive.zip)
> - After download just unzip the folder and open the index.html file in your browser.
> - ***Simple like that***

----------
