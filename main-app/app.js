let contentToolsToDoList = angular.module('contentToolsToDoList', ['ui-notification']); //Module declaration

contentToolsToDoList.config(function(NotificationProvider) {
    NotificationProvider.setOptions({
        delay: 4000,
        startTop: 40,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'top',
        positionY: 'top'
    });
});
